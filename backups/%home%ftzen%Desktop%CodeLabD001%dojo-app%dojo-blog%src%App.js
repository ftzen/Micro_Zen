import './App.css';
import Navbar from "./components/Navbar"
function App() {
  return (
    <div className="App">
 		<Navbar />
 		<div className="content">
 			<h1> App Component </h1>
 		</div>
    </div>
  );
}

export default App;
 
